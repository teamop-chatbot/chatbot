#######################################################
## Modules
#######################################################

import chatbot
import argparse
from flask import Flask, render_template, request, g, current_app
from random import choice as r_choice


#######################################################
## Testing the model
#######################################################

def test_model():
    details=True
    try:
        intents = chatbot.load_intents()
    except FileNotFoundError:
        print("File with training conversations 'intents.json' not found.")
        exit(1)
    try:
        words, classes, train_x, train_y = chatbot.retrieve_training_data()
    except FileNotFoundError:
        print("Missing training data. Try 'generate' first")
        exit(0)
    try:
        model = chatbot.load_model()
    except:
        print("Model missing. Try 'generate' first")
        exit(0)

    test_list=['kan jeg bruge mail til at sygemelde mig?',
               'Må jeg komme for sent?',
               'Hvor syg skal man være for at melde sig syg?',
               'Tak',
               'Står alt det her andre steder?',
               'Hvornår skal jeg møde?',
               'Hvor melder jeg mig syg?',
               'Hvad er en sygdomssamtale?',
               'Hvad er en sygefraværssamtale?',
               'Farvel',
               'Goodag',
               'Goddag',
               'Hvor længe må man være syg?',
               'Hvilke retningslinjer gælder for sygdom?',
               'Hvem skal jeg kontakte omkring sygdom?',
               'Hvordan foregår en sygefraværssamtale?',
               'Har I en manual i stedet for det her?',
               'Hvem er min chef?',
               'Må jeg sende sms?',
               'Jeg er syg',
               'Hvad skal jeg gøre?',
               'Må jeg sende en sms om det?',
               'Farvel',
               'Goddag',
               'Jeg er syg',
               'Tak',
               'Syg',
               'Det er mig',
               'Tak',
               'Syg',
               'Mit barn',
               'Farvel',
               'Halløj',
               'Hej',
               'Kan man låne udstyr?',
               'gopro',
               'Tak',
               'udlån',
               'spejlrefleks',
               'tak',
               'Jeg  har en aftale med Henrik',
               'Glem det']
    for testq in test_list:
        # print(testq) ## output now handled by chatbot itself
        if details: print(chatbot.classify(testq, words, classes, model))
        resp = chatbot.response(testq, intents, words, classes, model, show_details=details)
        # print(resp) ## output now handled by chatbot itself
        print()


#######################################################
## Chatbot as a web service - Class definition
#######################################################

app = Flask(__name__)

@app.before_first_request
def chatbot_initialization():
    app.details = True
    try:
        app.intents = chatbot.load_intents()
    except FileNotFoundError:
        print("File with training conversations 'intents.json' not found.")
        exit(1)
    try:
        app.words, app.classes, app.train_x, app.train_y = chatbot.retrieve_training_data()
    except FileNotFoundError:
        print("Missing training data. Try 'generate' first")
        exit(0)
    try:
        app.model = chatbot.load_model()
    except:
        print("Model missing. Try 'generate' first")
        exit(0)

#def get_response(msg):
#    responses = ["Hello.",
#                 "Malfunction.",
#                 "Crossed Wires.",
#                 "*Zapp!*",
#                 "Hi there. Do I know you?",
#                 "User Unrecognized...?",
#                 "Exterminate!!!",
#                 "Greetings."]
#    resp = r_choice(responses)
#    return resp

@app.route("/")
def home():
    return render_template("index.html")

@app.route("/get")
def get_bot_response():
    user_input = request.args.get('msg')

    if current_app.details: print(chatbot.classify(user_input,
                                                   current_app.words,
                                                   current_app.classes,
                                                   current_app.model))
    resp = chatbot.response(user_input,
                            current_app.intents,
                            current_app.words,
                            current_app.classes,
                            current_app.model,
                            show_details=current_app.details)

    return str(resp)


#######################################################
## Command-line handling
#######################################################

if __name__ == "__main__":
    command_hlp = \
        "Command to execute; either of:\n"\
        "generate: Generate new knowledge graph from training data\n"\
        "test:     Perform automated test of chatbot\n"\
        "run:      Run chatbot as a web server on local host"
    parser = argparse.ArgumentParser(description="Set up and run a chatbot service", epilog="Happy chatting")
    parser.add_argument("command", help=command_hlp)
    args = parser.parse_args()

    if args.command == "generate":
        chatbot.generate_model()
    elif args.command == "test":
        test_model()
    elif args.command == "run":
        app.run(port=9000)
