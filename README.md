
Chatbot Service
================

Not-so-simple chatbot service for answering a diverse array of questions in a given domain.
The chatbot is context-aware and will answer questions related to a previous subject if one such has been established during the conversation.
Questions can be phrased in various ways and will still be recognized, even if they do not resemble the training questions exactly.

This application is intended to be run from a server, and to provide a web-based chat service.

Installation
-------------

### Platform requirements

    Python      3.9.2
    Pip3       20.3.4
    Virtualenv 20.4.0
    Supervisor 4.2.2-2

### Set up environment

    mkdir prjname
    cd prjname
    virtualenv prjname_env
    source prjname_env/bin/activate

    git clone https://gitlab.com/teamop-chatbot/chatbot.git
    mv repname prjname_prj
    cd prjname_prj
    pip install -r requirements.txt --no-cache-dir

### Train and test chatbot

    python cb.py generate
    python cb.py test

### Run chatbot as a web service

    gunicorn --bind=<port> --workers=3 cb:app

### Set up automated chatbot web server

#### Set up Supervisor

Create `chatbot.conf` in `/etc/supervisor/conf.d`:

    [program:chatbot]
    directory=/home/teamop/chatbot/chatbot_prj
    command=/home/teamop/chatbot/chatbot_env/bin/gunicorn -w 3 chatbot:app
    user=teamop
    autostart=true
    autorestart=true
    stopasgroup=true
    killasgroup=true
    stderr_logfile=/var/log/simplebot/chatbot.err.log
    stdout_logfile=/var/log/simplebot/chatbot.out.log

Thus, supervisor should start the chatbot service with gunicorn at
startup or after `supervisorctl reload`; status is shown with
`supervisorctl status`.

Also create some files and directories, the ones the config tells
Supervisor to use, because otherwise it might get confused: Create dir
`chatbot` in `/var/log` and `touch` the two files
`chatbot.err.log` and `chatbot.out.log` within it.

    sudo mkdir /var/log/chatbot
    sudo touch /var/log/chatbot/chatbot.err.log
    sudo touch /var/log/chatbot/chatbot.out.log

#### Set up Nginx

Next, have Nginx refer incoming requests for the chatbot to the Gunicorn
process by identifying it with its port number.

`chatbot` in `/etc/nginx/sites-available` (linked to from
../sites-enabled):

    server {
            listen 80;
            server_name <ip-addr or host name>;

            location /static {
                    alias /home/teamop/chatbot/static;
            }

            location / {
                    proxy_pass http://localhost:8000;
                    include /etc/nginx/proxy_params;
                    proxy_redirect off;
            }
    }

The reference to /static up there is due to Nginx needing a place to
find resources like stylesheets for the rendered html, and while
gunicorn and python are many things, they are not that.
